module.exports = {
  extends: 'airbnb-base',
  rules: {
    'no-console': 0,
    'arrow-parens': 0,
    'new-cap': 0,
    'func-names': 0,
    'no-underscore-dangle': 0,
    'quote-props': 0,
    'radix': 0,
    'arrow-body-style': 0,
    'class-methods-use-this': 0,
    'consistent-return': 0,
    'no-await-in-loop': 0,
  },
};
