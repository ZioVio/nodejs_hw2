require('./config/env')();

const express = require('express');

const { port } = require('./config/server');
const { dbConnectionRetryCount, dbConnectionRetryTimeout } = require('./config/db');
const connectToDB = require('./db/connect');
const errorHandler = require('./middlewares/errorHandler');

const defaultMiddleware = require('./middlewares');
const routes = require('./routes');
const retry = require('./utils/retry');

const app = express();

app.use(defaultMiddleware);
app.use('/api', routes);
app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});

console.log('Connecting to db...');
retry(connectToDB, dbConnectionRetryCount, dbConnectionRetryTimeout)
  .then(() => {
    console.log('Connected to db');
  })
  .catch((err) => {
    console.log(`Failed to connect to db. ${err.message}`);
  });
