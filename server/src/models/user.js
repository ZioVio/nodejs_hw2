const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  username: { type: String, required: true, trim: true },
  password: { type: String },
  createdDate: { type: Date, default: Date.now },
}, { versionKey: false });

module.exports = mongoose.model('user', userSchema);
