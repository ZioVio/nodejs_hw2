const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  userId: { type: mongoose.SchemaTypes.ObjectId, ref: 'user', required: true },
  completed: { type: Boolean, default: false },
  text: { type: String, default: null, trim: true },
  createdDate: { type: Date, default: Date.now },
}, { versionKey: false });

module.exports = mongoose.model('note', noteSchema);
