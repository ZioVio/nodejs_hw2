const mongoose = require('mongoose');
const { dbUrl } = require('../config/db');

module.exports = async () => {
  return mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });
};
