const BaseStorage = require('./baseStorage');
const noteModel = require('../../models/note');

class NotesStorage extends BaseStorage {
  async getUserNotes(userId) {
    return this.model.find({ userId }).sort({ createdDate: -1 });
  }
}

module.exports = new NotesStorage(noteModel);
