const BaseStorage = require('./baseStorage');
const userModel = require('../../models/user');

class UsersStorage extends BaseStorage {
  async getByUsername(username) {
    return this.getOne({ username });
  }
}

module.exports = new UsersStorage(userModel);
