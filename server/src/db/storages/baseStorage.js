class BaseStorage {
  constructor(model) {
    this.model = model;
  }

  async get(query) {
    return this.model.find(query);
  }

  async getOne(query) {
    return this.model.findOne(query);
  }

  async getById(id) {
    return this.model.findById(id);
  }

  async getAll() {
    return this.model.find({});
  }

  async create(entity) {
    return new this.model(entity).save();
  }

  async deleteById(id) {
    return this.model.findByIdAndDelete(id);
  }

  async updateById(id, entity) {
    return this.model.findOneAndUpdate({ _id: id }, entity);
  }
}

module.exports = BaseStorage;
