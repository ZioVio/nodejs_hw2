const usersStorage = require('../db/storages/usersStorage');
const createError = require('../utils/createError');
const { compare, hashFunc } = require('../utils/hash');

class UsersService {
  async getUserById(id) {
    return usersStorage.getById(id);
  }

  async deleteUserById(id) {
    const user = await usersStorage.getById(id);
    if (!user) {
      throw createError(400, 'User is already deleted');
    }
    return usersStorage.deleteById(id);
  }

  async changeUserPassword(userId, oldPassword, newPassword) {
    const user = await this.getUserById(userId);
    if (!user) {
      throw this._createUserNotFoundError(userId);
    }
    const oldPasswordIsValid = compare(oldPassword, user.password);
    if (!oldPasswordIsValid) {
      throw createError(400, 'Incorrect old password');
    }
    const newHashedPass = hashFunc(newPassword);
    user.password = newHashedPass;
    return usersStorage.updateById(userId, user);
  }

  _createUserNotFoundError(id) {
    return createError(400, `User with id "${id}" not found`);
  }
}

module.exports = new UsersService();
