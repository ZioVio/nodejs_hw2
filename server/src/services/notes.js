const notesStorage = require('../db/storages/notesStorage');
const createError = require('../utils/createError');

class NotesService {
  async getUserNotes(userId) {
    return notesStorage.getUserNotes(userId);
  }

  async createNoteForUser(userId, note) {
    return notesStorage.create({ ...note, userId });
  }

  async getUserNote(userId, noteId) {
    return notesStorage.getOne({ userId, _id: noteId });
  }

  async updateNote(noteId, note) {
    return notesStorage.updateById(noteId, note);
  }

  async changeCheckedState(noteId) {
    const note = await notesStorage.getById(noteId);
    note.completed = !note.completed;
    return notesStorage.updateById(noteId, note);
  }

  async checkNoteBelongingToUser(noteId, userId) {
    const note = await notesStorage.getById(noteId);
    if (!note) {
      throw createError(400, `Cannot find note with id "${noteId}"`);
    }
    if (note.userId.toString() !== userId.toString()) {
      throw createError(403, 'You can view, update and delete only your notes');
    }
  }

  async deleteNote(noteId) {
    return notesStorage.deleteById(noteId);
  }
}

module.exports = new NotesService();
