const userStorage = require('../db/storages/usersStorage');
const createError = require('../utils/createError');
const { hashFunc, compare } = require('../utils/hash');
const { createToken } = require('../utils/jwt');

class AuthService {
  async register(creds) {
    const password = hashFunc(creds.password);
    const userWithSameUsername = await userStorage.getByUsername(creds.username);
    if (userWithSameUsername) {
      throw createError(400, `User with the same username "${creds.username}" already found`);
    }
    return userStorage.create({ ...creds, password });
  }

  async login(creds) {
    const possibleUser = await userStorage.getByUsername(creds.username);
    if (!possibleUser) {
      throw createError(400, `User with username "${creds.username}" not found`);
    }
    const passwordsEqual = compare(creds.password, possibleUser.password);
    if (!passwordsEqual) {
      throw createError(400, 'Invalid password');
    }
    const token = createToken({ userId: possibleUser._id, username: possibleUser.username });
    return token;
  }
}

module.exports = new AuthService();
