// eslint-disable-next-line no-unused-vars
module.exports = (err, req, res, next) => {
  const status = err.status || 500;
  if (status >= 500) {
    console.log(err);
  }
  const message = Array.isArray(err.messages) ? err.messages[0] : err.message;
  res.status(status).json({ message });
};
