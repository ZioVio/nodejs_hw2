const expressAsyncHandler = require('express-async-handler');
const createResponseMessage = require('../utils/createResponseMessage');
const { verifyToken } = require('../utils/jwt');
const { isString } = require('../utils/typeValidation');

module.exports.authenticate = async (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (!isString(authHeader)) {
    return res.status(401).json(createResponseMessage('Not authorized'));
  }
  const [, token] = authHeader.split(' ');
  try {
    const { userId } = verifyToken(token);
    req.userId = userId;
    return next();
  } catch (err) {
    return res.status(401).json(createResponseMessage('Session expired or invalid token'));
  }
};

module.exports.checkCredsMiddleware = expressAsyncHandler((req, res, next) => {
  if (!req.body) {
    return res.status(400).json(createResponseMessage('Request body is required'));
  }
  const { username, password } = req.body;
  if (!username || !password) {
    return res.status(400).json(createResponseMessage('Missing username of password'));
  }
  if (!isString(username) || !isString(password)) {
    return res.status(400).json(createResponseMessage('Username and password must be strings'));
  }
  return next();
});
