const { isLikeObjectId } = require('../utils/typeValidation');
const createResponse = require('../utils/createResponseMessage');

module.exports.validateId = (req, res, next) => {
  const { id } = req.params;
  if (!isLikeObjectId(id)) {
    return res.status(400).json(createResponse('Invalid object id'));
  }
  next();
};

module.exports.trimBodyForAllowedFields = (fields) => (req, res, next) => {
  if (!req.body) {
    return res.status(400).json(createResponse('Body cannot be empty'));
  }
  const validatedBody = fields.reduce((acc, key) => ({ ...acc, [key]: req.body[key] }), {});
  req.body = validatedBody;
  next();
};
