const expressAsyncHandler = require('express-async-handler');
const notesService = require('../services/notes');
const { isString } = require('../utils/typeValidation');
const createResponseMessage = require('../utils/createResponseMessage');

module.exports.checkExistenceAndBelongingToUser = expressAsyncHandler(async (req, res, next) => {
  const noteId = req.params.id;
  const { userId } = req;
  await notesService.checkNoteBelongingToUser(noteId, userId);
  next();
});

module.exports.noteBodyChecker = expressAsyncHandler(async (req, res, next) => {
  const note = req.body;
  if (!note || !isString(note.text)) {
    return res.status(400).json(createResponseMessage('Note text must be a string'));
  }
  next();
});
