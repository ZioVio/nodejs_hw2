const express = require('express');

const router = express.Router();
const cors = require('cors');

const dbConnectionChecker = require('./dbConnectionChecker');

const { isDev } = require('../config/server');

router.use(cors());
router.use(dbConnectionChecker);
router.use(express.json());

if (isDev) {
  // require only if in dev mode
  // should avoid non global require but is ok here
  // eslint-disable-next-line global-require
  const morgan = require('morgan');
  router.use(morgan('dev'));
}

module.exports = router;
