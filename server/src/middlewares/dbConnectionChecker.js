const mongoose = require('mongoose');
const createResponseMessage = require('../utils/createResponseMessage');

module.exports = (req, res, next) => {
  const isDbConnected = mongoose.connection.readyState === 1;
  if (isDbConnected) {
    return next();
  }
  res.status(503).json(createResponseMessage('Server unavailable, please try later'));
};
