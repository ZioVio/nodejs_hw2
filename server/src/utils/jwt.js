const jwt = require('jsonwebtoken');
const { jwtSecret, expiresIn } = require('../config/auth');

module.exports.createToken = (data) => jwt.sign(data, jwtSecret, { expiresIn });
module.exports.verifyToken = (token) => jwt.verify(token, jwtSecret);
