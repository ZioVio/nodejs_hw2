const mongoose = require('mongoose');

const isString = s => typeof s === 'string';
const isNumber = n => typeof n === 'number';
const isLikeObjectId = id => {
  try {
    mongoose.mongo.ObjectId(id);
    return true;
  } catch (err) {
    return false;
  }
};

module.exports = {
  isString, isNumber, isLikeObjectId,
};
