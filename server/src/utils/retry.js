const delayPromise = (err, val, timeout) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (err) {
        reject(err);
      } else {
        resolve(val);
      }
    }, timeout);
  });
};

const retry = async (asyncFunc, count, timeout) => {
  for (let i = count; i > 0; i -= 1) {
    try {
      const result = await asyncFunc();
      return result;
    } catch (err) {
      if (i === 1) {
        throw err;
      }
      await delayPromise(null, null, timeout);
    }
  }
};

module.exports = retry;
