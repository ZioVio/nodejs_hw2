const crypto = require('crypto');
const { passwordsSalt } = require('../config/auth');

const sha512 = function (password, salt) {
  const hash = crypto.createHmac('sha512', salt);
  hash.update(password);
  const value = hash.digest('hex');
  return value;
};

const hashFunc = string => sha512(string, passwordsSalt);
const compare = (string, hashed) => hashFunc(string) === hashed;

module.exports = {
  hashFunc, compare,
};
