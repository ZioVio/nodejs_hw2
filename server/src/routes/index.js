const express = require('express');

const router = express.Router();

const authRoutes = require('./auth');
const usersRoutes = require('./users');
const notesRoutes = require('./notes');

router.use('/auth', authRoutes);
router.use('/users', usersRoutes);
router.use('/notes', notesRoutes);

module.exports = router;
