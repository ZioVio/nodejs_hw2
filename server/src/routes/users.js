const express = require('express');

const router = express.Router();

const { authenticate } = require('../middlewares/auth');

const {
  onChangePassword,
  onDeleteCurrentUser,
  onGetCurrentUser,
} = require('../controllers/user');

router.use(authenticate);

router.get('/me', onGetCurrentUser);
router.delete('/me', onDeleteCurrentUser);
router.patch('/me', onChangePassword);

module.exports = router;
