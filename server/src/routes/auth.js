const express = require('express');

const router = express.Router();

const {
  register,
  login,
} = require('../controllers/auth');

const { checkCredsMiddleware } = require('../middlewares/auth');

router.post('/register', checkCredsMiddleware, register);
router.post('/login', checkCredsMiddleware, login);

module.exports = router;
