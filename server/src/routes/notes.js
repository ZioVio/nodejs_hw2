const express = require('express');
const { authenticate } = require('../middlewares/auth');

const {
  onGetUserNotes,
  onAddNote,
  onGetUserNote,
  onUpdateNote,
  onChangeNoteCheckedState,
  onDeleteNote,
} = require('../controllers/notes');

const { trimBodyForAllowedFields } = require('../middlewares/shared');
const { checkExistenceAndBelongingToUser, noteBodyChecker } = require('../middlewares/notes');
const { validateId } = require('../middlewares/shared');

const router = express.Router();

router.use(authenticate);

router.get('/', onGetUserNotes);
router.post('/', trimBodyForAllowedFields(['text']), noteBodyChecker, onAddNote);

router.use('/:id', validateId, checkExistenceAndBelongingToUser);

router.get('/:id', onGetUserNote);
router.put('/:id', trimBodyForAllowedFields(['text']), noteBodyChecker, onUpdateNote);
router.patch('/:id', onChangeNoteCheckedState);
router.delete('/:id', onDeleteNote);

module.exports = router;
