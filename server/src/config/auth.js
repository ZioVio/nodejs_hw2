module.exports = {
  passwordsSalt: process.env.PASSWORDS_SALT,
  jwtSecret: process.env.JWT_SECRET,
  expiresIn: process.env.JWT_EXPIRES_IN || '1d',
};
