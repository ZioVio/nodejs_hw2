const isProd = process.env.NODE_ENV === 'PROD' || process.env.NODE_ENV === 'PRODUCTION';
const isDev = !isProd;

module.exports = {
  port: process.env.PORT || 8080,
  isProd,
  isDev,
};
