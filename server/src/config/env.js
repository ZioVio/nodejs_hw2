const path = require('path');
const dotenv = require('dotenv');
const fs = require('fs');

const dotEnvPath = path.join(__dirname, '../../', '.env');
const dotEnvExamplePath = `${dotEnvPath}.example`;

module.exports = () => {
  if (fs.existsSync(dotEnvPath)) {
    dotenv.config({ path: dotEnvPath });
  } else {
    dotenv.config({ path: dotEnvExamplePath });
  }
};
