const dbName = process.env.DB_NAME;
const dbHost = process.env.DB_HOST;
const dbPort = process.env.DB_PORT;

const readyDBUrl = process.env.DB_URL;

module.exports = {
  dbUrl: readyDBUrl || `mongodb://${dbHost}:${dbPort}/${dbName}`,
  dbConnectionRetryTimeout: parseInt(process.env.DB_CONNECTION_RETRY_TIMEOUT) || 500,
  dbConnectionRetryCount: parseInt(process.env.DB_CONNECTION_RETRY_COUNT) || 10,
};
