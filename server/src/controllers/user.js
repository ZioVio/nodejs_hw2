const expressAsyncHandler = require('express-async-handler');

const createResponseMessage = require('../utils/createResponseMessage');

const usersService = require('../services/users');
const { isString } = require('../utils/typeValidation');

module.exports.onGetCurrentUser = expressAsyncHandler(async (req, res) => {
  const { userId } = req;
  const user = await usersService.getUserById(userId);
  if (!user) {
    return res.status(400).json(createResponseMessage('Failed to get user, try to relogin'));
  }
  const responseUser = {
    _id: user._id,
    username: user.username,
    createdDate: user.createdDate,
  };
  res.json({ user: responseUser });
});

module.exports.onDeleteCurrentUser = expressAsyncHandler(async (req, res) => {
  const { userId } = req;
  await usersService.deleteUserById(userId);
  res.json(createResponseMessage());
});

module.exports.onChangePassword = expressAsyncHandler(async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  if (!oldPassword || !newPassword) {
    return res.status(400).json(createResponseMessage('Missing old or new password'));
  }
  if (!isString(oldPassword) || !isString(newPassword)) {
    return res.status(400).json(createResponseMessage('Passwords must be strings'));
  }
  await usersService.changeUserPassword(req.userId, oldPassword, newPassword);
  res.json(createResponseMessage());
});
