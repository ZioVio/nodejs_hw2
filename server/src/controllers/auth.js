const expressAsyncHandler = require('express-async-handler');

const createResponseMessage = require('../utils/createResponseMessage');
const authService = require('../services/auth');

module.exports.register = expressAsyncHandler(async (req, res) => {
  await authService.register(req.body);
  res.json(createResponseMessage());
});

module.exports.login = expressAsyncHandler(async (req, res) => {
  const token = await authService.login(req.body);
  res.json({ jwt_token: token });
});
