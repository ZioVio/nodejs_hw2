const expressAsyncHandler = require('express-async-handler');
const notesService = require('../services/notes');
const createResponseMessage = require('../utils/createResponseMessage');

module.exports.onGetUserNotes = expressAsyncHandler(async (req, res) => {
  const notes = await notesService.getUserNotes(req.userId);
  res.json({ notes });
});

module.exports.onAddNote = expressAsyncHandler(async (req, res) => {
  const note = req.body;
  await notesService.createNoteForUser(req.userId, note);
  res.status(201).json(createResponseMessage());
});

module.exports.onGetUserNote = expressAsyncHandler(async (req, res) => {
  const noteId = req.params.id;
  const { userId } = req;
  const note = await notesService.getUserNote(userId, noteId);
  res.json({ note });
});

module.exports.onUpdateNote = expressAsyncHandler(async (req, res) => {
  const { id } = req.params;
  const { userId } = req;
  await notesService.updateNote(id, req.body, userId);
  res.json(createResponseMessage());
});

module.exports.onChangeNoteCheckedState = expressAsyncHandler(async (req, res) => {
  const { id } = req.params;
  await notesService.changeCheckedState(id);
  res.json(createResponseMessage());
});

module.exports.onDeleteNote = expressAsyncHandler(async (req, res) => {
  const { id } = req.params;
  await notesService.deleteNote(id);
  res.json(createResponseMessage());
});
