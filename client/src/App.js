import React, { useState } from 'react';
import { BrowserRouter } from 'react-router-dom';
import './App.css';
import Navbar from './components/layout/navbar';
import Auth from './components/pages/auth';
import Wrapper from './components/pages/wrapper';
import AuthContext from './contexts/authContext';
import AuthService from './services/auth.service';

function App() {
  const [user, setGlobalUser] = useState(AuthService.getUser());
  return (
    <>
      <AuthContext.Provider value={{
        getUser: () => user,
        setUser: (u) => {
          AuthService.setUser(u);
          setGlobalUser(u);
        },
        logout: () => {
          AuthService.logout();
          setGlobalUser(null);
        },
      }}>
        <BrowserRouter>
          <Navbar />
          <Wrapper />
        </BrowserRouter>
      </AuthContext.Provider>
    </>
  );
}

export default App;
