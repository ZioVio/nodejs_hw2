import authService from '../services/auth.service';

const getAuthHeader = () => `Bearer ${authService.getToken()}`;

const getAuthHeaderObject = () => {
  const authHeader = getAuthHeader();
  return authHeader ? { authorization: authHeader } : {};
};

const getContentTypeHeaderObject = () => ({
  'Content-type': 'application/json',
});

const getHeaders = () => ({ ...getAuthHeaderObject(), ...getContentTypeHeaderObject() });

const stringifyBody = body => (typeof body === 'string' ? body : JSON.stringify(body));

const parseResponseBody = res => {
  const contentTypeHeader = res.headers.get('Content-Type');

  const hasBody = !!res.headers.get('Content-Length');
  const isJSON = hasBody && contentTypeHeader && contentTypeHeader.includes('application/json');

  if (hasBody && isJSON) {
    return res.json().then(parsedBody => ({ status: res.status, body: parsedBody, ok: res.ok }));
  }
  return { status: res.status, body: null, ok: res.ok };
};

const checkResponseForErr = (res) => {
  if (!res.ok) {
    return Promise.reject((new Error(res.body.message)));
  }
  return res.body;
};

export const makeRequestWithBody = (path, body, method) => {
  return fetch(path, {
    headers: getHeaders(),
    body: stringifyBody(body),
    method,
  })
    .then(parseResponseBody)
    .then(checkResponseForErr);
};

export const makeRequestWithoutBody = (path, method = 'GET') => {
  return fetch(path, {
    headers: getHeaders(),
    method,
  })
    .then(parseResponseBody)
    .then(checkResponseForErr);
};
