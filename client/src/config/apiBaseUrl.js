const isProd = process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'prod';
const port = process.env.PORT || 8080;

export default isProd ? 'api' : `http://localhost:${port}/api`;
