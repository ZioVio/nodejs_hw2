const constraints = {
  maxPasswordLength: 20,
  maxUsernameLength: 50,
  maxNoteTextLength: 300,
};

export default constraints;
