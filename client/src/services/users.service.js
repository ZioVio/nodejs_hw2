import HttpService from './http.service';
import BaseService from './base.service';
import AuthService from './auth.service';

class UsersService extends BaseService {
  async changePassword(oldPassword, newPassword) {
    return HttpService.patch(`${this.baseUrl}me`, {
      oldPassword, newPassword,
    });
  }
}

export default new UsersService('/users/');
