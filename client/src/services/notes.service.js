import HttpService from './http.service';
import BaseService from './base.service';

class NotesService extends BaseService {
  constructor(route) {
    super(route);
    this.cachedNotes = null;
  }

  async getNotes() {
    if (this.cachedNotes) {
      return this.cachedNotes;
    }
    const { notes } = await HttpService.get(this.baseUrl);
    this.cachedNotes = notes;
    return notes;
  }

  async addNote(note) {
    await HttpService.post(this.baseUrl, note);
    this.cachedNotes = null;
  }

  async getNoteById(id) {
    if (this.cachedNotes) {
      return this.cachedNotes.find(note => note._id === id);
    }
    const { note } = HttpService.get(`${this.baseUrl}${id}`);
    return note;
  }

  async updateNote(updatedNode) {
    const id = updatedNode._id;
    await HttpService.put(`${this.baseUrl}${id}`, updatedNode);
    if (this.cachedNotes) {
      this.cachedNotes = this.cachedNotes
        .map(note => (note._id === id ? { ...updatedNode } : note));
    }
  }

  async changeCheckedState(id) {
    await HttpService.patch(`${this.baseUrl}${id}`);
    if (this.cachedNotes) {
      this.cachedNotes = this.cachedNotes
        .map(note => (note._id === id ? { ...note, completed: !note.completed } : note));
    }
  }

  async deleteNote(id) {
    await HttpService.delete(`${this.baseUrl}${id}`);
    this.cachedNotes = this.cachedNotes.filter(note => note._id !== id);
  }

  async clearCache() {
    this.cachedNotes = null;
  }
}

export default new NotesService('/notes/');
