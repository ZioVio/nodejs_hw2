import baseApiUrl from '../config/apiBaseUrl';

export default class BaseService {
  constructor(apiRoute) {
    this.baseUrl = baseApiUrl + apiRoute;
  }
}
