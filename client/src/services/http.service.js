import {
  makeRequestWithoutBody,
  makeRequestWithBody,
} from '../utils/http';

class HttpService {
  get(path) {
    return makeRequestWithoutBody(path);
  }

  post(path, body) {
    return makeRequestWithBody(path, body, 'POST');
  }

  patch(path, body) {
    return makeRequestWithBody(path, body, 'PATCH');
  }

  put(path, body) {
    return makeRequestWithBody(path, body, 'PUT');
  }

  delete(path) {
    return makeRequestWithoutBody(path, 'DELETE');
  }
}

export default new HttpService();
