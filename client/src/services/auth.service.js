import jwtDecode from 'jwt-decode';
import BaseService from './base.service';
import HttpService from './http.service';
import NotesService from './notes.service';

class AuthService extends BaseService {
  constructor(baseUrl) {
    super(baseUrl);
    this.tokenStorageKey = 'token';
    this.setUser(this.getUser());
  }

  setUser(user) {
    this.user = user;
  }

  getUser() {
    if (!this.user) {
      const token = this.getToken();
      if (!token) {
        return null;
      }
      try {
        const possibleCachedUser = jwtDecode(token);
        this.setUser(possibleCachedUser);
        return possibleCachedUser;
      } catch (err) {
        this.setUser(null);
        return null;
      }
    }
    return this.user;
  }

  getToken() {
    return localStorage.getItem(this.tokenStorageKey);
  }

  setToken(token) {
    localStorage.setItem(this.tokenStorageKey, token);
    this.setUser(null);
  }

  logout() {
    this.user = null;
    this._clearCacheAfterLogout();
  }

  _clearCacheAfterLogout() {
    NotesService.clearCache();
    localStorage.removeItem(this.tokenStorageKey);
  }

  async authenticate(username, password) {
    const response = await HttpService.post(`${this.baseUrl}login`, {
      username: username.trim(),
      password,
    });
    const jwt = response.jwt_token;
    const user = jwtDecode(jwt);
    this.setToken(jwt);
    return user;
  }

  async register(username, password) {
    return HttpService.post(`${this.baseUrl}register`, {
      username: username.trim(),
      password,
    });
  }
}

export default new AuthService('/auth/');
