import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import AuthContext from '../../contexts/authContext';

export default function Navbar(props) {
  const { getUser, logout, setUser } = useContext(AuthContext);
  const [user, setUserState] = useState(getUser());
  const onLogout = () => {
    logout();
    setUser(null);
  };

  useEffect(() => {
    setUserState(getUser());
  }, [getUser()]);

  return (
    <nav className="navbar navbar-light bg-light">
      <Link to="/home">
        <span className="navbar-brand">@TODO app</span>
      </Link>
      {user
        ? <UserDropdown user={user} onLogout={onLogout} />
        : <LoginBtn />}
    </nav>
  );
}

function UserDropdown({ user, onLogout, onChangePassword }) {
  if (!user) {
    return (
      <></>
    );
  }
  return (
    <div className="btn-group dropleft">
      <button type="button" className="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {user.username}
        </button>
      <div className="dropdown-menu">
        <Link to="/password-change">
          <a className="dropdown-item" href="#" onClick={onChangePassword}>Change password</a>
        </Link>
        <div className="dropdown-divider"></div>
        <Link to="/auth">
          <a className="dropdown-item" href="#" onClick={onLogout}>Logout</a>
        </Link>
      </div>
    </div>
  );
}

function LoginBtn() {
  return (
    <div className="d-flex">
      <Link to="/auth">
        <button className="btn btn-outline-secondary mx-2">Log in</button>
      </Link>
    </div>
  );
}
