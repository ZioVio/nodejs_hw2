/* eslint-disable no-restricted-globals */
/* eslint-disable no-alert */
import React, { useContext, useEffect, useState } from 'react';
import moment from 'moment';
import { Redirect } from 'react-router';
import { useForm } from 'react-hook-form';

import NotesService from '../../services/notes.service';
import Error from '../utils/error';
import AuthContext from '../../contexts/authContext';
import Loader from '../utils/loader';
import inputConstraints from '../../config/inputConstraints';

export default function Home() {
  const [notes, setNotes] = useState(null);
  const [loadingNotes, setLoadingNotes] = useState(true);

  const { getUser } = useContext(AuthContext);
  if (!getUser()) {
    return <Redirect to="/auth" />;
  }

  const [errMessage, setErrMessage] = useState(null);

  const onNotesRecieved = (recievedNotes) => {
    setNotes(recievedNotes);
  };

  const onErr = (err) => {
    setErrMessage(err.message);
  };

  const loadNotes = () => {
    return NotesService.getNotes()
      .then(onNotesRecieved)
      .catch(onErr)
      .then(() => setLoadingNotes(false));
  };

  const onAddNote = (note) => {
    setLoadingNotes(true);
    return NotesService.addNote(note)
      .then(() => loadNotes())
      .catch(onErr)
      .then(() => setLoadingNotes(false));
  };

  const onDeleteNote = (note) => {
    // only for demo. Will never use this in prod =)
    const id = note._id;
    const shouldDelete = confirm('Delete current note?');
    if (shouldDelete) {
      setLoadingNotes(true);
      NotesService.deleteNote(id)
        .then(loadNotes)
        .catch(onErr)
        .then(() => setLoadingNotes(false));
    }
  };

  const onChangeCheckedState = (note) => {
    const id = note._id;
    setLoadingNotes(true);
    NotesService.changeCheckedState(id)
      .then(loadNotes)
      .catch(onErr)
      .then(() => setLoadingNotes(false));
  };

  useEffect(() => {
    loadNotes();
  }, []);

  return (
    <div className="card p-2 m-2">
      <NotesList
        notes={notes}
        loading={loadingNotes}
        onAddNote={onAddNote}
        onDelete={onDeleteNote}
        onChangeCheckedState={onChangeCheckedState} />
    </div>
  );
}

function Note({ note, isEditable, onCheck, disabled, onDelete, onChangeCheckedState }) {
  return (
    <div className="card m-2">
      <div className="card-body">
        <div className="row">
          <div className="col-8">
            <p className={`card-text ${note.completed ? 'text-line-through' : ''}`}>
              {note.text}
            </p>
          </div>
          <div className="col-4 d-flex justify-content-center align-items-center">
            <button
              className="btn btn-outline-primary"
              disabled={disabled}
              onClick={() => onChangeCheckedState(note)}>
              {note.completed ? 'Uncheck' : 'Check'}
            </button>
          </div>
        </div>
      </div>
      <div className="card-footer text-muted">
        <div className="row">
          <div className="col-8">
            <p className="p-0 m-0">{moment(note.createdDate).fromNow()}</p>
          </div>
          <div className="col-4 d-flex justify-content-around">
            <button
              className="btn btn-sm text-muted p-0 mr-2"
              onClick={() => onDelete(note)}
              disabled={disabled}>Delete</button>
          </div>
        </div>
      </div>
    </div>
  );
}

function NotesList({ notes, onAddNote, loading, onDelete, onChangeCheckedState }) {
  if (!notes) {
    return <></>;
  }

  const {
    register,
    handleSubmit,
    reset,
    errors } = useForm();

  const onSubmit = (note) => {
    onAddNote(note).then(() => reset());
  };

  return (
    <>
      <form className="form m-2" onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group">
          <textarea className="form-control" id="text" name="text" ref={register({
            required: 'Text required',
            maxLength: {
              value: inputConstraints.maxNoteTextLength,
              message: `Max length is ${inputConstraints.maxNoteTextLength}`,
            },
          })} />
        </div>
        <button type="submit" className="btn btn-primary btn-block" disabled={loading}>
          {loading ? <Loader /> : 'Add note'}
        </button>
        <Error msg={errors.text && errors.text.message} />
      </form>
      {notes.length
        ? notes
          .map(note => <Note
            key={note._id}
            note={note}
            disabled={loading}
            onDelete={onDelete}
            onChangeCheckedState={onChangeCheckedState} />)
        : <p className="p-3">No notes yet. Go ahead and add some!</p>}
    </>
  );
}
