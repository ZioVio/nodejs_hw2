import React, { useContext, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import inputConstraints from '../../config/inputConstraints';
import AuthContext from '../../contexts/authContext';
import UserService from '../../services/users.service';
import Error from '../utils/error';
import Success from '../utils/success';

export default function PasswordChange() {
  const {
    register,
    handleSubmit,
    errors,
    reset,
  } = useForm();
  const { logout } = useContext(AuthContext);

  const history = useHistory();

  const [loading, setLoading] = useState(false);

  const [errMessage, setErrMessage] = useState(null);
  const [successMessage, setSuccessMessage] = useState(null);

  const onSuccessChangePass = () => {
    reset();
    setSuccessMessage('Successfully changed password. You were logged out');
    logout();
  };

  const onFailedToChangePass = ({ message }) => {
    setErrMessage(message);
  };

  const onSubmit = ({ oldPassword, newPassword }) => {
    setErrMessage(null);
    setSuccessMessage(null);
    setLoading(true);
    UserService.changePassword(oldPassword, newPassword)
      .then(onSuccessChangePass)
      .catch(onFailedToChangePass)
      .then(() => setLoading(false));
  };

  const onGoBack = () => {
    history.goBack();
  };

  return (
    <>
      <div className="card p-2 m-2">
        <form className="form" onSubmit={handleSubmit(onSubmit)}>
          <div className="form-group">
            <label htmlFor="oldPassword">Old password</label>
            <input id="oldPassword" name="oldPassword" type="password" className="form-control" disabled={loading}
              ref={
                register({
                  required: 'Old password required',
                  maxLength: {
                    value: inputConstraints.maxPasswordLength,
                    message: `Max length is ${inputConstraints.maxPasswordLength}`,
                  },
                })
              }
            ></input>
            <div className="text-danger">{errors.oldPassword && errors.oldPassword.message}</div>
          </div>
          <div className="form-group">
            <label htmlFor="newPassword">New password</label>
            <input id="newPassword" name="newPassword" type="password" className="form-control" disabled={loading}
              ref={
                register({
                  required: 'New password required',
                  maxLength: {
                    value: inputConstraints.maxPasswordLength,
                    message: `Max length is ${inputConstraints.maxPasswordLength}`,
                  },
                })
              }
            ></input>
            <div className="text-danger">{errors.newPassword && errors.newPassword.message}</div>
          </div>
          <div className="d-flex justify-content-center">
            <button type="button" className="btn" onClick={() => onGoBack()}>Go back</button>
            <button type="submit" className="btn btn-outline-primary mx-2" disabled={loading}>Change</button>
          </div>
        </form>
        <div className="text-center">
          <Error msg={errMessage} />
          <Success msg={successMessage} />
        </div>
      </div>
    </>
  );
}
