import React from 'react';
import {
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import PasswordChange from './passwordChange';
import Auth from './auth';
import Home from './home';

export default function Wrapper() {
  return (
    <div className="container-md content d-flex justify-content-center">
      <Switch>
        <Route path="/auth">
          <Auth />
        </Route>
        <Route path="/password-change">
          <PasswordChange />
        </Route>
        <Route path="/home">
          <Home />
        </Route>
        <Redirect to="/home" />
      </Switch>
    </div>
  );
}
