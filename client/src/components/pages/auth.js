import React, { useContext, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router';
import AuthService from '../../services/auth.service';
import inputConstraints from '../../config/inputConstraints';
import AuthContext from '../../contexts/authContext';
import Loader from '../utils/loader';
import Success from '../utils/success';
import Error from '../utils/error';

function onRegister({ username, password }) {
  return AuthService.register(username, password)
    .catch(err => Promise.reject(err.message));
}

function onLogin({ username, password }) {
  return AuthService.authenticate(username, password)
    .catch(err => Promise.reject(err.message));
}

export default function Auth() {
  const {
    register,
    handleSubmit,
    errors,
    reset,
  } = useForm();
  const history = useHistory();
  const { setUser } = useContext(AuthContext);

  const [isLoading, setLoading] = useState(false);
  const [isLogin, setIsLogin] = useState(false);

  const [errMessage, setErrMessage] = useState(null);
  const [successMessage, setSuccessMessage] = useState(null);

  const onSuccessLogin = (user) => {
    setUser(user);
    setErrMessage(null);
    setSuccessMessage('Successfully logged in');
    setTimeout(() => history.push('/home'), 2000);
  };

  const onSuccessRegister = (res) => {
    reset();
    setErrMessage(null);
    setSuccessMessage('Successfully signed up. Login to continue');
    setLoading(false);
  };

  const onErr = (msg) => {
    setSuccessMessage(null);
    setErrMessage(msg);
    setLoading(false);
  };

  const onSubmit = (value) => {
    setLoading(true);
    if (isLogin) {
      onLogin(value)
        .then(onSuccessLogin)
        .catch(onErr);
    } else {
      onRegister(value)
        .then(onSuccessRegister)
        .catch(onErr);
    }
  };
  return (
    <>
      <div className="card p-2 m-2">
        <form className="form" onSubmit={handleSubmit(onSubmit)}>
          <div className="form-group">
            <label htmlFor="username">Username</label>
            <input id="username" name="username" type="text" className="form-control" disabled={isLoading}
              ref={
                register({
                  required: 'Username required',
                  maxLength: {
                    value: inputConstraints.maxUsernameLength,
                    message: `Max length is ${inputConstraints.maxUsernameLength}`,
                  },
                })
              }
            ></input>
            <Error msg={errors.username && errors.username.message} />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input id="password" name="password" type="password" className="form-control" disabled={isLoading}
              ref={
                register({
                  required: 'Password required',
                  maxLength: {
                    value: inputConstraints.maxPasswordLength,
                    message: `Max password length is ${inputConstraints.maxPasswordLength}`,
                  },
                })
              }
            ></input>
            <Error msg={errors.password && errors.password.message} />
          </div>
          <div className="d-flex justify-content-center">
            <button type="submit" className="btn btn-outline-primary mx-2" disabled={isLoading}
              onClick={() => setIsLogin(false)}>Sign up</button>
            <button type="submit" className="btn btn-outline-secondary mx-2" disabled={isLoading}
              onClick={() => setIsLogin(true)}>Log in</button>
          </div>
          <div className="text-center mt-2">
            <Error msg={errMessage} />
            <Success msg={successMessage} />
          </div>
        </form>
        {isLoading
          ? <div className="text-center">
              <Loader />
            </div>
          : null
        }
      </div>
    </>
  );
}
