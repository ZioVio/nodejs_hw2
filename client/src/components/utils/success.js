import React from 'react';

export default function Success({ msg }) {
  return (
    msg ? <span className="text-success">{msg}</span> : null
  );
}
