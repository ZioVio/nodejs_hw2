import React from 'react';

export default function Error({ msg }) {
  return (
    msg ? <span className="text-danger">{msg}</span> : null
  );
}
