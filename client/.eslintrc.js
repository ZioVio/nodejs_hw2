module.exports = {
  extends: [
    'plugin:react/recommended',
    '../.eslintrc.js',
  ],
  root: true,
  env: {
    browser: true,
    es6: true,
  },
  parserOptions: {
    ecmaVersion: 2018,
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    'import/no-named-as-default': 0,
    'import/no-named-as-default-member': 0,
    'import/no-cycle': 0,
    'no-unused-vars': 0,
    'react/prop-types': 0,
    'object-curly-newline': 0,
  },
};
